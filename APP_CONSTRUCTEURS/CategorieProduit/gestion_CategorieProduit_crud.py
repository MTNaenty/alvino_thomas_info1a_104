"""
    Fichier : gestion_CategorieProduit_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les CategorieProduit.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_CONSTRUCTEURS import obj_mon_application
from APP_CONSTRUCTEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_CONSTRUCTEURS.erreurs.exceptions import *
from APP_CONSTRUCTEURS.erreurs.msg_erreurs import *
from APP_CONSTRUCTEURS.CategorieProduit.gestion_CategorieProduit_wtf_forms import FormWTFAjouterCategorieProduit
from APP_CONSTRUCTEURS.CategorieProduit.gestion_CategorieProduit_wtf_forms import FormWTFDeleteCategorieProduit
from APP_CONSTRUCTEURS.CategorieProduit.gestion_CategorieProduit_wtf_forms import FormWTFUpdateCategorieProduit

"""
    Nom : CategorieProduit_afficher
    Auteur : TA 17.05.2021
    Définition d'une "route" /CategorieProduit_crud

    But : Afficher les CategorieProduit avec les categories associés pour chaque constructeur.

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_CategorieProduit_sel = 0 >> toutes les categories.
                ID_CategorieProduit_sel = "n" affiche les categories dont l'ID est "n"

"""


@obj_mon_application.route("/CategorieProduit_afficher/<string:order_by>/<int:ID_CategorieProduit_sel>", methods=['GET', 'POST'])
def CategorieProduit_afficher(order_by, ID_CategorieProduit_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion CategorieProduit ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe CategorieProduit Gestion CategorieProduit {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_CategorieProduit_sel == 0:
                    print("1")
                    strsql_CategorieProduit_afficher = """SELECT ID_CategorieProduit, TypeCategorieProduit FROM t_categorieproduit ORDER BY ID_CategorieProduit ASC"""
                    mc_afficher.execute(strsql_CategorieProduit_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_constructeur"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'ID du constructeur sélectionné avec un nom de variable
                    valeur_ID_CategorieProduit_selected_dictionnaire = {"value_ID_CategorieProduit_selected": ID_CategorieProduit_sel}
                    strsql_CategorieProduit_afficher = """SELECT ID_CategorieProduit, TypeCategorieProduit FROM t_categorieproduit  WHERE ID_CategorieProduit = %(value_ID_CategorieProduit_selected)s"""

                    mc_afficher.execute(strsql_CategorieProduit_afficher, valeur_ID_CategorieProduit_selected_dictionnaire)
                else:
                    strsql_CategorieProduit_afficher = """SELECT ID_CategorieProduit, TypeCategorieProduit FROM t_categorieproduit ORDER BY ID_CategorieProduit DESC"""

                    mc_afficher.execute(strsql_CategorieProduit_afficher)

                data_CategorieProduit = mc_afficher.fetchall()

                print("data_CategorieProduit ", data_CategorieProduit, " Type : ", type(data_CategorieProduit))

                # Différencier les messages si la table est vide.
                if not data_CategorieProduit and ID_CategorieProduit_sel == 0:
                    flash("""La table "t_categorieproduit" est vide. !!""", "warning")
                elif not data_CategorieProduit and ID_CategorieProduit_sel > 0:
                    # Si l'utilisateur change l'ID_CategorieProduit dans l'URL et que le CategorieProduit n'existe pas,
                    flash(f"La categorie demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_categorieproduit" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données CategorieProduit affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. CategorieProduit_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} CategorieProduit_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("CategorieProduit/CategorieProduit_afficher.html", data=data_CategorieProduit)


"""
    Auteur : Jésus 00.00.0000
    Définition d'une "route" /CategorieProduit_ajouter
    
    Test : ex : http://127.0.0.1:5005/CategorieProduit_ajouter
    
    Paramètres : sans
    
    But : Ajouter une CategorieProduit pour un article
    
    Remarque :  Dans le champ "name_CategorieProduit_html" du formulaire "CategorieProduit/CategorieProduit_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/CategorieProduit_ajouter", methods=['GET', 'POST'])
def CategorieProduit_ajouter_wtf():
    form = FormWTFAjouterCategorieProduit()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion CategorieProduit ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe CategorieProduit GestionCategorieProduit {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_CategorieProduit_wtf = form.TypeCategorieProduit_wtf.data

                name_CategorieProduit = name_CategorieProduit_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_TypeCategorieProduit": name_CategorieProduit}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_CategorieProduit = """INSERT INTO t_categorieproduit (ID_CategorieProduit,TypeCategorieProduit) VALUES (NULL,%(value_TypeCategorieProduit)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_CategorieProduit, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('CategorieProduit_afficher', order_by='DESC', ID_CategorieProduit_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_CategorieProduit_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_CategorieProduit_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion CategorieProduit CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("CategorieProduit/CategorieProduit_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /CategorieProduit_update
    
    Test : ex cliquer sur le menu "CategorieProduit" puis cliquer sur le bouton "EDIT" d'un "CategorieProduit"
    
    Paramètres : sans
    
    But : Editer(update) un CategorieProduit qui a été sélectionné dans le formulaire "CategorieProduit_afficher.html"
    
    Remarque :  Dans le champ "CategorieProduit_update_wtf" du formulaire "CategorieProduit/CategorieProduit_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/CategorieProduit_update", methods=['GET', 'POST'])
def CategorieProduit_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_CategorieProduit"
    ID_CategorieProduit_update = request.values['ID_CategorieProduit_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateCategorieProduit()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "Constructeurs_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_CategorieProduit_update = form_update.TypeCategorieProduit_update_wtf.data
            name_CategorieProduit_update = name_CategorieProduit_update.lower()

            valeur_update_dictionnaire = {"value_ID_CategorieProduit": ID_CategorieProduit_update,
                                          "value_name_CategorieProduit": name_CategorieProduit_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_TypeCategorieProduit = """UPDATE t_categorieproduit SET TypeCategorieProduit = %(value_name_CategorieProduit)s WHERE ID_CategorieProduit = %(value_ID_CategorieProduit)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_TypeCategorieProduit, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_Constructeur_update"
            return redirect(url_for('CategorieProduit_afficher', order_by="ASC", ID_CategorieProduit_sel=ID_CategorieProduit_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_CategorieProduit" et "TypeCategorieProduit" de la "t_categorieproduit"
            str_sql_ID_CategorieProduit = "SELECT ID_CategorieProduit, TypeCategorieProduit FROM t_categorieproduit WHERE ID_CategorieProduit = %(value_ID_CategorieProduit)s"
            valeur_select_dictionnaire = {"value_ID_CategorieProduit": ID_CategorieProduit_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_CategorieProduit, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'UPDATE
            data_TypeCategorieProduit = mybd_curseur.fetchone()
            print("data_TypeCategorieProduit ", data_TypeCategorieProduit, " type ", type(data_TypeCategorieProduit), "CategorieProduit",
                  data_TypeCategorieProduit["TypeCategorieProduit"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "Constructeurs_update_wtf.html"
            form_update.TypeCategorieProduit_update_wtf.data = data_TypeCategorieProduit["TypeCategorieProduit"]

    # Jesus 2020.04.16 avant JC ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans CategorieProduit_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans CategorieProduit_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans CategorieProduit_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans CategorieProduit_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("CategorieProduit/CategorieProduit_update_wtf.html", form_update=form_update)


"""
    Auteur : TA 02.06.2021
    Définition d'une "route" /CategorieProduit_delete
    
    Test : ex. cliquer sur le menu "CategorieProduit" puis cliquer sur le bouton "DELETE" d'un "CategorieProduit"
    
    Paramètres : sans
    
    But : Effacer(delete) une CategorieProduit qui a été sélectionné dans le formulaire "CategorieProduit_afficher.html"
    
    Remarque :  Dans le champ "CategorieProduit_delete_wtf" du formulaire "CategorieProduit/CategorieProduit_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/CategorieProduit_delete", methods=['GET', 'POST'])
def CategorieProduit_delete_wtf():
    data_TypeCategorieProduit_attribue_CategorieProduit_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_Constructeur"
    ID_CategorieProduit_delete = request.values['ID_CategorieProduit_btn_delete_html']

    # Objet formulaire pour effacer le constructeur sélectionné.
    form_delete = FormWTFDeleteCategorieProduit()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("CategorieProduit_afficher", order_by="ASC", ID_CategorieProduit_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "CategorieProduit/CategorieProduit_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_TypeCategorieProduit_attribue_CategorieProduit_delete = session['data_TypeCategorieProduit_attribue_CategorieProduit_delete']
                print("data_TypeCategorieProduit_attribue_CategorieProduit_delete ", data_TypeCategorieProduit_attribue_CategorieProduit_delete)

                flash(f"Effacer le CategorieProduit de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer constructeur" qui va irrémédiablement EFFACER le constructeur
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_CategorieProduit": ID_CategorieProduit_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_TypeCategorieProduit_CategorieProduit = """DELETE FROM t_categorieproduit WHERE ID_CategorieProduit = %(value_ID_CategorieProduit)s"""
                str_sql_delete_ID_CategorieProduit = """DELETE FROM t_categorieproduit WHERE ID_CategorieProduit = %(value_ID_CategorieProduit)s"""
                # Manière brutale d'effacer d'abord la "fk_constructeur", même si elle n'existe pas dans la "t_ggenre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_TypeCategorieProduit_CategorieProduit, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_ID_CategorieProduit, valeur_delete_dictionnaire)

                flash(f"Categorie définitivement effacé !!", "success")
                print(f"Categorie définitivement effacé !!")

                # afficher les données
                return redirect(url_for('CategorieProduit_afficher', order_by="ASC", ID_CategorieProduit_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_CategorieProduit": ID_CategorieProduit_delete}
            print(ID_CategorieProduit_delete, type(ID_CategorieProduit_delete))

            # Requête qui affiche tous les constructeurs qui ont la categorie que l'utilisateur veut effacer
            str_sql_CategorieProduit_TypeCategorieProduit_delete = """SELECT TypeCategorieProduit FROM t_categorieproduit
INNER JOIN t_avoircategorieproduit ON t_categorieproduit.ID_CategorieProduit = t_avoircategorieproduit.FK_ID_CategorieProduit
WHERE t_categorieproduit.ID_CategorieProduit = %(value_ID_CategorieProduit)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_CategorieProduit_TypeCategorieProduit_delete, valeur_select_dictionnaire)
            data_TypeCategorieProduit_attribue_CategorieProduit_delete = mybd_curseur.fetchall()
            print("data_TypeCategorieProduit_attribue_CategorieProduit_delete...", data_TypeCategorieProduit_attribue_CategorieProduit_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "constructeurs/Constructeurs_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_TypeCategorieProduit_attribue_CategorieProduit_delete'] = data_TypeCategorieProduit_attribue_CategorieProduit_delete

            # Opération sur la BD pour récupérer "ID_Constructeur" et "NomConstructeur" de la "t_constructeur"
            str_sql_ID_CategorieProduit = "SELECT ID_CategorieProduit, TypeCategorieProduit FROM t_categorieproduit WHERE ID_CategorieProduit= %(value_ID_CategorieProduit)s"

            mybd_curseur.execute(str_sql_ID_CategorieProduit, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'action DELETE
            data_TypeCategorieProduit = mybd_curseur.fetchone()
            print("data_TypeCategorieProduit ", data_TypeCategorieProduit, " type ", type(data_TypeCategorieProduit), " categorie de produit ",
                  data_TypeCategorieProduit["TypeCategorieProduit"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "Constructeurs_delete_wtf.html"
            form_delete.TypeCategorieProduit_delete_wtf.data = data_TypeCategorieProduit["TypeCategorieProduit"]

            # Le bouton pour l'action "DELETE" dans le form. "Constructeurs_delete_wtf.html" est caché.
            btn_submit_del = False

    # Dieu2 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans CategorieProduit_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans CategorieProduit_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans CategorieProduit_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans CategorieProduit_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("CategorieProduit/CategorieProduit_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_TypeCategorieProduit_associes=data_TypeCategorieProduit_attribue_CategorieProduit_delete)
