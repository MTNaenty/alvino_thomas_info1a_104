"""
    Fichier : gestion_CategorieProduit_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterCategorieProduit(FlaskForm):
    """
        Dans le formulaire "AvoirCategorieProduit_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    TypeCategorieProduit_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    TypeCategorieProduit_wtf = StringField("Clavioter la categorie ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(TypeCategorieProduit_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer categorie")


class FormWTFUpdateCategorieProduit(FlaskForm):
    """
        Dans le formulaire "AvoirCategorieProduit_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    TypeCategorieProduit_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    TypeCategorieProduit_update_wtf = StringField("Clavioter la categorie ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(TypeCategorieProduit_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Update categorieproduit")


class FormWTFDeleteCategorieProduit(FlaskForm):
    """
        Dans le formulaire "CategorieProduit_delete_wtf.html"

        TypeCategorieProduit_delete_wtf : Champ qui reçoit la valeur du CategorieProduit, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "CategorieProduit".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_categorieproduit".
    """
    TypeCategorieProduit_delete_wtf = StringField("Effacer ctte categorie")
    submit_btn_del = SubmitField("Effacer categorie")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
