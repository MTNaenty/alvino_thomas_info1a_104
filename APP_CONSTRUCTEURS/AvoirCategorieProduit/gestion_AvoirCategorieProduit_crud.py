"""
    Fichier : AvoirCategorieProduit_genres_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_CONSTRUCTEURS import obj_mon_application
from APP_CONSTRUCTEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_CONSTRUCTEURS.erreurs.exceptions import *
from APP_CONSTRUCTEURS.erreurs.msg_erreurs import *
from APP_CONSTRUCTEURS.AvoirCategorieProduit.gestion_AvoirCategorieProduit_wtf_forms import FormWTFAjouterAvoirConstructeurs
from APP_CONSTRUCTEURS.AvoirCategorieProduit.gestion_AvoirCategorieProduit_wtf_forms import FormWTFDeleteAvoirConstructeurs
from APP_CONSTRUCTEURS.AvoirCategorieProduit.gestion_AvoirCategorieProduit_wtf_forms import FormWTFUpdateAvoirConstructeurs

"""
    Nom : AvoirCategorieProduit_afficher
    Auteur : OVFDSAFEW 20dewdwd1.05.ddlolbite01
    Définition d'une "route" /AvoirCategorieProduit_afficher
    
    But : Afficher les trucs avec les categories associées pour chaque constructeur.
    
    Paramètres : ID_AvoirCategorieProduit_sel = 0 >> tous les films.
                 ID_AvoirCategorieProduit_sel = "n" affiche le film dont l'id est "n"
                 
"""


@obj_mon_application.route("/AvoirCategorieProduit_afficher/<int:ID_AvoirCategorieProduit_sel>", methods=['GET', 'POST'])
def AvoirCategorieProduit_afficher(ID_AvoirCategorieProduit_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_AvoirCategorieProduit_afficher:
                code, msg = Exception_init_AvoirCategorieProduit_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_AvoirCategorieProduit_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_AvoirCategorieProduit_afficher.args[0]} , "
                      f"{Exception_init_AvoirCategorieProduit_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_AvoirCategorieProduit_afficher_data = """SELECT * from t_avoircategorieproduit
                INNER JOIN t_categorieproduit ON t_categorieproduit .ID_CategorieProduit = t_avoircategorieproduit.FK_ID_CategorieProduit
                INNER JOIN t_constructeur ON t_constructeur .ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur"""
                if ID_AvoirCategorieProduit_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_AvoirCategorieProduit_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_AvoirCategorieProduit_selected_dictionnaire = {"value_ID_AvoirCategorieProduit_selected": ID_AvoirCategorieProduit_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_AvoirCategorieProduit_afficher_data += """ HAVING ID_AvoirCategorieProduit= %(value_ID_AvoirCategorieProduit_selected)s"""

                    mc_afficher.execute(strsql_AvoirCategorieProduit_afficher_data, valeur_id_AvoirCategorieProduit_selected_dictionnaire)

                # Récupère les données de la requête.
                data_AvoirCategorieProduit_afficher = mc_afficher.fetchall()
                print("data_CategorieProduit ", data_AvoirCategorieProduit_afficher, " Type : ", type(data_AvoirCategorieProduit_afficher))

                # Différencier les messages.
                if not data_AvoirCategorieProduit_afficher and ID_AvoirCategorieProduit_sel == 0:
                    flash("""La table "t_avoircategorieproduit" est vide. !""", "warning")
                elif not data_AvoirCategorieProduit_afficher and ID_AvoirCategorieProduit_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le constructeur {ID_AvoirCategorieProduit_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données constructeurs et categories affichés !!", "success")

        except Exception as Exception_AvoirCategorieProduit_afficher:
            code, msg = Exception_AvoirCategorieProduit_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception AvoirCategorieProduit_afficher : {sys.exc_info()[0]} "
                  f"{Exception_AvoirCategorieProduit_afficher.args[0]} , "
                  f"{Exception_AvoirCategorieProduit_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("AvoirCategorieProduit/AvoirCategorieProduit_afficher.html", data=data_AvoirCategorieProduit_afficher)


"""
    nom: edit_AvoirCategorieProduit_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "films_genres_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_genre".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_AvoirCategorieProduit_selected", methods=['GET', 'POST'])
def edit_AvoirCategorieProduit_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_CategorieProduit_afficher = """SELECT * FROM t_constructeur ORDER BY ID_Constructeur ASC"""
                mc_afficher.execute(strsql_CategorieProduit_afficher)
            data_CategorieProduit_all = mc_afficher.fetchall()
            print("dans edit_AvoirCategorieProduit_selected ---> data_CategorieProduit_all", data_CategorieProduit_all)

            # Récupère la valeur de "id_film" du formulaire html "ID_ModifierConstructeurs_btn_edit_html.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_ModifierCategorieProduit_edit" dans le fichier "films_genres_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', id_film_genres_edit_html=row.id_film) }}"
            id_ModifierCategorieProduit_edit = request.values['ID_ModifierConstructeurs_btn_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_AvoirCategorieProduit_genres_edit'] = id_ModifierCategorieProduit_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_ID_AvoirCategorieProduit_selected_dictionnaire = {
                "value_ID_AvoirCategorieProduit_selected": id_ModifierCategorieProduit_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction genres_films_afficher_data
            # 1) Sélection du constructeur choisi
            # 2) Sélection des categories "déjà" attribuées pour le constructeur.
            # 3) Sélection des categories "pas encore" attribuées pour le constructeur choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "AvoirCategorieProduit_afficher_data"
            data_Constructeur_selected, data_CategorieProduit_attribues, data_CategorieProduit_non_attribues = \
                AvoirCategorieProduit_afficher_data(valeur_ID_AvoirCategorieProduit_selected_dictionnaire)

            print(data_Constructeur_selected)
            lst_data_Constructeur_selected = [item['ID_Constructeur'] for item in
                                              data_Constructeur_selected]
            print("lst_data_Constructeur_selected  ", lst_data_Constructeur_selected,
                  type(lst_data_Constructeur_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_Constructeur_non_attribues = [item['NomConstructeur'] for item in
                                                   data_CategorieProduit_non_attribues]
            session['session_lst_data_Constructeur_non_attribues'] = lst_data_Constructeur_non_attribues
            print("lst_data_Constructeur_non_attribues  ", lst_data_Constructeur_non_attribues,
                  type(lst_data_Constructeur_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_Constructeur_old_attribues = [item['NomConstructeur'] for item in
                                                   data_CategorieProduit_attribues]
            session['session_lst_data_Constructeur_old_attribues'] = lst_data_Constructeur_old_attribues
            print("lst_data_Constructeur_old_attribues  ", lst_data_Constructeur_old_attribues,
                  type(lst_data_Constructeur_old_attribues))

            print(" data data_AvoirCategorieProduit_selected", data_Constructeur_selected, "type ",
                  type(data_Constructeur_selected))
            print(" data data_AvoirCategorieProduit_non_attribues ", data_CategorieProduit_non_attribues, "type ",
                  type(data_CategorieProduit_non_attribues))
            print(" data_AvoirCategorieProduit_attribues ", data_CategorieProduit_attribues, "type ",
                  type(data_CategorieProduit_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_Constructeur_non_attribues = [item['NomConstructeur'] for item in
                                                   data_CategorieProduit_non_attribues]
            print("lst_all_CategorieProduit gf_edit_AvoirCategorieProduit_selected ",
                  lst_data_Constructeur_non_attribues,
                  type(lst_data_Constructeur_non_attribues))

        except Exception as Exception_edit_AvoirCategorieProduit_selected:
            code, msg = Exception_edit_AvoirCategorieProduit_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_AvoirCategorieProduit_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_AvoirCategorieProduit_selected.args[0]} , "
                  f"{Exception_edit_AvoirCategorieProduit_selected}", "danger")

    return render_template("AvoirCategorieProduit/AvoirCategorieProduit_modifier_tags_dropbox.html",
                           data_CategorieProduit=data_CategorieProduit_all,
                           lst_data_Constructeur_selected=data_Constructeur_selected,
                           data_CategorieProduit_attribues=data_CategorieProduit_attribues,
                           data_CategorieProduit_non_attribues=data_CategorieProduit_non_attribues)


"""
    nom: update_AvoirCategorieProduit_selected

    Récupère la liste de tous les genres du film sélectionné par le bouton "MODIFIER" de "films_genres_afficher.html"

    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les genres contenus dans la "t_genre".
    2) Les genres attribués au film selectionné.
    3) Les genres non-attribués au film sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_AvoirCategorieProduit_selected", methods=['GET', 'POST'])
def update_AvoirCategorieProduit_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            ID_AvoirCategorieProduit_selected = session['session_id_AvoirCategorieProduit_edit']
            print("session['session_id_AvoirCategorieProduit_edit'] ", session['session_id_AvoirCategorieProduit_edit'])

            # Récupère la liste des genres qui ne sont pas associés au film sélectionné.
            old_lst_data_Constructeur_non_attribues = session['session_lst_data_Constructeur_non_attribues']
            print("old_lst_data_Constructeur_non_attribues ", old_lst_data_Constructeur_non_attribues)

            # Récupère la liste des genres qui sont associés au film sélectionné.
            old_lst_data_Constructeur_attribues = session['session_lst_data_Constructeur_old_attribues']
            print("old_lst_data_Constructeur_old_attribues ", old_lst_data_Constructeur_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme categories dans le composant "tags-selector-tagselect"
            # dans le fichier "AvoirCategorieProduit_modifier_tags_dropbox.html"
            new_lst_str_AvoirCategorieProduit = request.form.getlist('name_select_tags')
            print("new_lst_str_AvoirCategorieProduit ", new_lst_str_AvoirCategorieProduit)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_AvoirCategorieProduit_old = list(map(int, new_lst_str_AvoirCategorieProduit))
            print("new_lst_AvoirCategorieProduit ", new_lst_int_AvoirCategorieProduit_old, "type new_lst_AvoirCategorieProduit ",
                  type(new_lst_int_AvoirCategorieProduit_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "ID_CategorieProduit" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_CategorieProduit_delete_b = list(
                set(old_lst_data_Constructeur_attribues) - set(new_lst_int_AvoirCategorieProduit_old))
            print("lst_diff_CategorieProduit_delete_b ", lst_diff_CategorieProduit_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_CategorieProduit_insert_a = list(
                set(new_lst_int_AvoirCategorieProduit_old) - set(old_lst_data_Constructeur_attribues))
            print("lst_diff_CategorieProduit_insert_a ", lst_diff_CategorieProduit_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "FK_ID_Constructeur"/"ID_Constructeur" et "FK_ID_CategorieProduit"/"ID_CategorieProduit" dans la "t_avoircategorieproduit"
            strsql_insert_AvoirCategorieProduit = """INSERT INTO t_avoircategorieproduit (ID_AvoirCategorieProduit, FK_ID_CategorieProduit, FK_ID_Constructeur)
                                                    VALUES (NULL, %(value_FK_ID_CategorieProduit)s, %(value_FK_ID_Constructeur)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_AvoirCategorieProduit = """DELETE FROM t_avoircategorieproduit WHERE FK_ID_CategorieProduit = %(value_FK_ID_CategorieProduit)s AND FK_ID_Constructeur = %(value_FK_ID_Constructeur)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des genres à INSÉRER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for ID_CategorieProduit_ins in lst_diff_CategorieProduit_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du constructeur sélectionné avec un nom de variable
                    # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_AvoirCategorieProduit_sel_categorieproduit_sel_dictionnaire = {"value_FK_ID_Constructeur": ID_AvoirCategorieProduit_selected,
                                                               "value_FK_ID_CategorieProduit": ID_CategorieProduit_ins}

                    mconn_bd.mabd_execute(strsql_insert_AvoirCategorieProduit, valeurs_AvoirCategorieProduit_sel_categorieproduit_sel_dictionnaire)

                # Pour le constructeur sélectionné, parcourir la liste des genres à EFFACER dans la "t_AvoirCategorieProduit".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for ID_CategorieProduit_del in lst_diff_CategorieProduit_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du constructeur sélectionné avec un nom de variable
                    # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_AvoirCategorieProduit_sel_categorieproduit_sel_dictionnaire = {"value_FK_ID_Constructeur": ID_AvoirCategorieProduit_selected,
                                                               "value_FK_ID_CategorieProduit": ID_CategorieProduit_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_AvoirCategorieProduit, valeurs_AvoirCategorieProduit_sel_categorieproduit_sel_dictionnaire)

        except Exception as Exception_update_AvoirCategorieProduit_selected:
            code, msg = Exception_update_AvoirCategorieProduit_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_AvoirCategorieProduit_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_AvoirCategorieProduit_selected.args[0]} , "
                  f"{Exception_update_AvoirCategorieProduit_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_AvoirCategorieProduit",
    # on affiche les constructeurs et le(urs) genre(s) associé(s).
    return redirect(url_for('AvoirCategorieProduit_afficher', ID_AvoirCategorieProduit_sel=ID_AvoirCategorieProduit_selected))


"""
    nom: AvoirCategorieProduit_afficher_data

    Récupère la liste de tous les genres du constructeur sélectionné par le bouton "MODIFIER" de "AvoirCategorieProduit_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des genres, ainsi l'utilisateur voit les genres à disposition

    On signale les erreurs importantes
"""


def AvoirCategorieProduit_afficher_data(valeur_ID_AvoirCategorieProduit_selected_dict):
    print("valeur_ID_AvoirCategorieProduit_selected_dict...", valeur_ID_AvoirCategorieProduit_selected_dict)
    try:

        strsql_AvoirCategorieProduit_selected = """SELECT * FROM t_avoircategorieproduit
                                        INNER JOIN t_constructeur ON t_constructeur.ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur
                                        INNER JOIN t_CategorieProduit ON t_categorieproduit.ID_CategorieProduit = t_avoircategorieproduit.FK_ID_CategorieProduit
                                        WHERE FK_ID_Constructeur = %(value_FK_ID_Constructeur_selected)s"""

        strsql_AvoirCategorieProduit_non_attribues = """SELECT * FROM t_categorieproduit WHERE ID_CategorieProduit SELECT FK_ID_CategorieProduit FROM t_avoircategorieproduit
                                                    INNER JOIN t_constructeur ON t_constructeur.ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur
                                                    INNER JOIN t_categorieproduit ON t_categorieproduit.ID_CategorieProduit = t_avoircategorieproduit.FK_ID_CategorieProduit
                                                    WHERE FK_ID_CategorieProduit = %(value_FK_ID_Constructeur_selected)s)"""

        strsql_AvoirCategorieProduit_attribues = """SELECT * FROM t_avoircategorieproduit
                                            INNER JOIN t_constructeur ON t_constructeur.ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur
                                            INNER JOIN t_categorieproduit ON t_categorieproduit.ID_CategorieProduit = t_avoircategorieproduit.FK_ID_CategorieProduit
                                            WHERE FK_ID_Constructeur = %(value_FK_ID_Constructeur_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_AvoirCategorieProduit_non_attribues, valeur_ID_AvoirCategorieProduit_selected_dict)
            # Récupère les données de la requête.
            data_AvoirCategorieProduit_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("AvoirCategorieProduit_afficher_data ----> AvoirCategorieProduit_non_attribues ", data_AvoirCategorieProduit_non_attribues,
                  " Type : ",
                  type(data_AvoirCategorieProduit_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_AvoirCategorieProduit_selected, valeur_ID_AvoirCategorieProduit_selected_dict)
            # Récupère les données de la requête.
            data_AvoirCategorieProduit_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_AvoirCategorieProduit_selected  ", data_AvoirCategorieProduit_selected, " Type : ", type(data_AvoirCategorieProduit_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_AvoirCategorieProduit_attribues, valeur_ID_AvoirCategorieProduit_selected_dict)
            # Récupère les données de la requête.
            data_AvoirCategorieProduit_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_AvoirCategorieProduit_attribues ", data_AvoirCategorieProduit_attribues, " Type : ",
                  type(data_AvoirCategorieProduit_attribues))

            # Retourne les données des "SELECT"
            return data_AvoirCategorieProduit_selected, data_AvoirCategorieProduit_non_attribues, data_AvoirCategorieProduit_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans AvoirCategorieProduit_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans AvoirCategorieProduit_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_genres_AvoirCategorieProduit_data:
        code, msg = IntegrityError_genres_AvoirCategorieProduit_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans AvoirCategorieProduit_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_genres_AvoirCategorieProduit_data.args[0]} , "
              f"{IntegrityError_genres_AvoirCategorieProduit_data}", "danger")


@obj_mon_application.route("/AvoirConstructeurs_ajouter", methods=['GET', 'POST'])
def AvoirConstructeurs_ajouter_wtf():
    form = FormWTFAjouterAvoirConstructeurs()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion AvoirConstructeurs ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe AvoirConstructeurs GestionAvoirConstructeurs {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_AvoirConstructeurs_wtf = form.NomConstructeur_wtf.data

                name_AvoirConstructeurs = name_AvoirConstructeurs_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_NomConstructeur": name_AvoirConstructeurs}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_AvoirConstructeurs = """INSERT INTO t_constructeur (ID_Constructeur,NomConstructeur) VALUES (NULL,%(value_NomConstructeur)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_AvoirConstructeurs, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('AvoirCategorieProduit_afficher', order_by='DESC', ID_AvoirCategorieProduit_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_constructeur_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_constructeur_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion AvoirConstructeurs CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("AvoirCategorieProduit/AvoirConstructeurs_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /constructeur_update

    Test : ex cliquer sur le menu "constructeurs" puis cliquer sur le bouton "EDIT" d'un "constructeur"

    Paramètres : sans

    But : Editer(update) un constructeur qui a été sélectionné dans le formulaire "constructeurs_afficher.html"

    Remarque :  Dans le champ "NomConstructeur_update_wtf" du formulaire "constructeurs/Constructeurs_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/AvoirConstructeurs_update", methods=['GET', 'POST'])
def AvoirConstructeurs_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_Constructeur"
    ID_AvoirConstructeurs_update = request.values['ID_AvoirConstructeurs_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateAvoirConstructeurs()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "AvoirConstructeurs_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_AvoirConstructeurs_update = form_update.NomConstructeur_update_wtf.data
            name_AvoirConstructeurs_update = name_AvoirConstructeurs_update.lower()

            valeur_update_dictionnaire = {"value_ID_Constructeur": ID_AvoirConstructeurs_update,
                                          "value_name_constructeur": name_AvoirConstructeurs_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_NomConstructeur = """UPDATE t_constructeur SET NomConstructeur = %(value_name_constructeur)s WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_NomConstructeur, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_Constructeur_update"
            return redirect(
                url_for('AvoirCategorieProduit_afficher', order_by="ASC", ID_AvoirCategorieProduit_sel=ID_AvoirConstructeurs_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_Constructeur" et "NomConstructeur" de la "t_constructeur"
            str_sql_ID_AvoirConstructeurs = "SELECT ID_Constructeur, NomConstructeur FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"
            valeur_select_dictionnaire = {"value_ID_Constructeur": ID_AvoirConstructeurs_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_AvoirConstructeurs, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'UPDATE
            data_NomConstructeur = mybd_curseur.fetchone()
            print("data_NomConstructeur ", data_NomConstructeur, " type ", type(data_NomConstructeur), " constructeur ",
                  data_NomConstructeur["NomConstructeur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "Constructeurs_update_wtf.html"
            form_update.NomConstructeur_update_wtf.data = data_NomConstructeur["NomConstructeur"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans AvoirConstructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans AvoirConstructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans constructeurs_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans constructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("AvoirCategorieProduit/AvoirConstructeurs_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /AvoirConstructeurs_delete

    Test : ex. cliquer sur le menu "constructeurs" puis cliquer sur le bouton "DELETE" d'un "constructeur"

    Paramètres : sans

    But : Effacer(delete) un constructeur qui a été sélectionné dans le formulaire "constructeurs_afficher.html"

    Remarque :  Dans le champ "NomConstructeur_delete_wtf" du formulaire "constructeurs/Constructeurs_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/AvoirConstructeurs_delete", methods=['GET', 'POST'])
def AvoirConstructeurs_delete_wtf():
    data_NomConstructeur_attribue_AvoirConstructeurs_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_Constructeur"
    ID_AvoirConstructeurs_delete = request.values['ID_AvoirConstructeurs_btn_delete_html']

    # Objet formulaire pour effacer le constructeur sélectionné.
    form_delete = FormWTFDeleteAvoirConstructeurs()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("AvoirCategorieProduit_afficher", order_by="ASC", ID_AvoirCategorieProduit_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "AvoirConstructeurs/AvoirConstructeurss_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_NomConstructeur_attribue_AvoirConstructeurs_delete = session[
                    'data_NomConstructeur_attribue_AvoirConstructeurs_delete']
                print("data_NomConstructeur_attribue_AvoirConstructeurs_delete ",
                      data_NomConstructeur_attribue_AvoirConstructeurs_delete)

                flash(f"Effacer le constructeur de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer constructeur" qui va irrémédiablement EFFACER le constructeur
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_Constructeur": ID_AvoirConstructeurs_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_NomConstructeur_AvoirConstructeurs = """DELETE FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
                str_sql_delete_ID_AvoirConstructeurs = """DELETE FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
                # Manière brutale d'effacer d'abord la "fk_constructeur", même si elle n'existe pas dans la "t_ggenre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_NomConstructeur_AvoirConstructeurs, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_ID_AvoirConstructeurs, valeur_delete_dictionnaire)

                flash(f"Constructeur définitivement effacé !!", "success")
                print(f"Constructeur définitivement effacé !!")

                # afficher les données
                return redirect(url_for('AvoirCategorieProduit_afficher', order_by="ASC", ID_AvoirCategorieProduit_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_Constructeur": ID_AvoirConstructeurs_delete}
            print(ID_AvoirConstructeurs_delete, type(ID_AvoirConstructeurs_delete))

            # Requête qui affiche tous les films qui ont le constructeur que l'utilisateur veut effacer
            str_sql_AvoirConstructeurs_NomConstructeur_delete = """SELECT * FROM t_constructeur
INNER JOIN t_avoircategorieproduit ON t_constructeur.ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur
WHERE t_constructeur.ID_Constructeur = %(value_ID_Constructeur)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_AvoirConstructeurs_NomConstructeur_delete, valeur_select_dictionnaire)
            data_NomConstructeur_attribue_AvoirConstructeurs_delete = mybd_curseur.fetchall()
            print("data_NomConstructeur_attribue_constructeur_delete...",
                  data_NomConstructeur_attribue_AvoirConstructeurs_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "constructeurs/AvoirConstructeurs_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session[
                'data_NomConstructeur_attribue_AvoirConstructeurs_delete'] = data_NomConstructeur_attribue_AvoirConstructeurs_delete

            # Opération sur la BD pour récupérer "ID_Constructeur" et "NomConstructeur" de la "t_constructeur"
            str_sql_ID_AvoirConstructeurs = "SELECT * FROM t_constructeur WHERE ID_Constructeur= %(value_ID_Constructeur)s"

            mybd_curseur.execute(str_sql_ID_AvoirConstructeurs, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'action DELETE
            data_NomConstructeur = mybd_curseur.fetchone()
            print("data_NomConstructeur ", data_NomConstructeur, " type ", type(data_NomConstructeur), " constructeur ",
                  data_NomConstructeur["NomConstructeur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "AvoirConstructeurs_delete_wtf.html"
            form_delete.NomConstructeur_delete_wtf.data = data_NomConstructeur["NomConstructeur"]

            # Le bouton pour l'action "DELETE" dans le form. "CAvoirConstructeurs_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans AvoirConstructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans AvoirConstructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans AvoirConstructeurs_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans AvoirConstructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("AvoirCategorieProduit/AvoirConstructeurs_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_NomConstructeur_associes=data_NomConstructeur_attribue_AvoirConstructeurs_delete)
