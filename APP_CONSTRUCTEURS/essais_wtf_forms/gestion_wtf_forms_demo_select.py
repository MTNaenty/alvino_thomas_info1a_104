"""
    Fichier : gestion_constructeurs_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les constructeurs.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from APP_CONSTRUCTEURS import obj_mon_application
from APP_CONSTRUCTEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_CONSTRUCTEURS.erreurs.msg_erreurs import *
from APP_CONSTRUCTEURS.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /constructeur_delete
    
    Test : ex. cliquer sur le menu "constructeurs" puis cliquer sur le bouton "DELETE" d'un "constructeur"
    
    Paramètres : sans
    
    But : Effacer(delete) un constructeur qui a été sélectionné dans le formulaire "constructeurs_afficher.html"
    
    Remarque :  Dans le champ "NomConstructeur_delete_wtf" du formulaire "constructeurs/AvoirCategorieProduit_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    constructeur_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_constructeur"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_constructeur.data:

            if form_demo.submit_btn_ok_dplist_constructeur.data:
                print("Constructeur sélectionné : ",
                      form_demo.constructeurs_dropdown_wtf.data)
                constructeur_selectionne = form_demo.constructeurs_dropdown_wtf.data
                form_demo.constructeurs_dropdown_wtf.choices = session['constructeur_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_constructeurs_afficher = """SELECT ID_Constructeur, NomConstructeur FROM t_constructeur ORDER BY ID_Constructeur ASC"""
                mc_afficher.execute(strsql_constructeurs_afficher)

            data_constructeurs = mc_afficher.fetchall()
            print("demo_select_wtf data_constructeurs ", data_constructeurs, " Type : ", type(data_constructeurs))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            constructeur_val_list_dropdown = []
            for i in data_constructeurs:
                constructeur_val_list_dropdown.append(i['NomConstructeur'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # constructeur_val_list_dropdown = [(i["ID_Constructeur"], i["NomConstructeur"]) for i in data_constructeur]

            print("constructeur_val_list_dropdown ", constructeur_val_list_dropdown)

            form_demo.constructeurs_dropdown_wtf.choices = constructeur_val_list_dropdown
            session['constructeur_val_list_dropdown'] = constructeur_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.constructeurs_dropdown_wtf.data = "philosophique"
            constructeur_selectionne = form_demo.constructeurs_dropdown_wtf.data
            print("constructeur choisi dans la liste :", constructeur_selectionne)
            session['constructeur_selectionne_get'] = constructeur_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           constructeur_selectionne=constructeur_selectionne)
