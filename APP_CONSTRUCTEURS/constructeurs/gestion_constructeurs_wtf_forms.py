"""
    Fichier : gestion_CategorieProduit_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterConstructeurs(FlaskForm):
    """
        Dans le formulaire "AvoirCategorieProduit_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    NomConstructeur_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    NomConstructeur_wtf = StringField("Clavioter le constructeur ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(NomConstructeur_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer constructeur")


class FormWTFUpdateConstructeurs(FlaskForm):
    """
        Dans le formulaire "AvoirCategorieProduit_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    NomConstructeur_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    NomConstructeur_update_wtf = StringField("Clavioter le constructeur ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(NomConstructeur_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Update constructeur")


class FormWTFDeleteConstructeurs(FlaskForm):
    """
        Dans le formulaire "AvoirCategorieProduit_delete_wtf.html"

        NomConstructeur_delete_wtf : Champ qui reçoit la valeur du constructeur, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "constructeur".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_constructeur".
    """
    NomConstructeur_delete_wtf = StringField("Effacer ce constructeur")
    submit_btn_del = SubmitField("Effacer constructeur")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
