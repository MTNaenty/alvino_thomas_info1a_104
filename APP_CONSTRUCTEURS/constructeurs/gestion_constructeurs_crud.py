"""
    Fichier : gestion_constructeurs_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les constructeurs.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_CONSTRUCTEURS import obj_mon_application
from APP_CONSTRUCTEURS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_CONSTRUCTEURS.erreurs.exceptions import *
from APP_CONSTRUCTEURS.erreurs.msg_erreurs import *
from APP_CONSTRUCTEURS.constructeurs.gestion_constructeurs_wtf_forms import FormWTFAjouterConstructeurs
from APP_CONSTRUCTEURS.constructeurs.gestion_constructeurs_wtf_forms import FormWTFDeleteConstructeurs
from APP_CONSTRUCTEURS.constructeurs.gestion_constructeurs_wtf_forms import FormWTFUpdateConstructeurs

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /constructeurs_afficher
    
    Test : ex : http://127.0.0.1:5005/constructeurs_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_Constructeur_sel = 0 >> tous les constructeurs.
                ID_Constructeur_sel = "n" affiche le constructeur dont l'ID est "n"
"""


@obj_mon_application.route("/constructeurs_afficher/<string:order_by>/<int:ID_Constructeur_sel>", methods=['GET', 'POST'])
def constructeurs_afficher(order_by, ID_Constructeur_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion constructeurs ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionconstructeurs {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_Constructeur_sel == 0:
                    print("1")
                    strsql_constructeurs_afficher = """SELECT ID_Constructeur, NomConstructeur FROM t_constructeur ORDER BY ID_Constructeur ASC"""
                    mc_afficher.execute(strsql_constructeurs_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_constructeur"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'ID du constructeur sélectionné avec un nom de variable
                    valeur_ID_Constructeur_selected_dictionnaire = {"value_ID_Constructeur_selected": ID_Constructeur_sel}
                    strsql_constructeurs_afficher = """SELECT ID_Constructeur, NomConstructeur FROM t_constructeur  WHERE ID_Constructeur = %(value_ID_Constructeur_selected)s"""

                    mc_afficher.execute(strsql_constructeurs_afficher, valeur_ID_Constructeur_selected_dictionnaire)
                else:
                    strsql_constructeurs_afficher = """SELECT ID_Constructeur, NomConstructeur FROM t_constructeur ORDER BY ID_Constructeur DESC"""

                    mc_afficher.execute(strsql_constructeurs_afficher)

                data_constructeurs = mc_afficher.fetchall()

                print("data_constructeurs ", data_constructeurs, " Type : ", type(data_constructeurs))

                # Différencier les messages si la table est vide.
                if not data_constructeurs and ID_Constructeur_sel == 0:
                    flash("""La table "t_constructeur" est vide. !!""", "warning")
                elif not data_constructeurs and ID_Constructeur_sel > 0:
                    # Si l'utilisateur change l'ID_Constructeur dans l'URL et que le constructeur n'existe pas,
                    flash(f"Le constructeur demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_constructeur" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données constructeurs affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. constructeurs_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} constructeurs_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("constructeurs/constructeurs_afficher.html", data=data_constructeurs)


"""
    Auteur : Jésus 00.00.0000
    Définition d'une "route" /constructeurs_ajouter
    
    Test : ex : http://127.0.0.1:5005/constructeurs_ajouter
    
    Paramètres : sans
    
    But : Ajouter un constructeur pour un article
    
    Remarque :  Dans le champ "name_constructeur_html" du formulaire "constructeurs/constructeurs_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/constructeurs_ajouter", methods=['GET', 'POST'])
def constructeurs_ajouter_wtf():
    form = FormWTFAjouterConstructeurs()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion constructeurs ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionConstructeurs {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_constructeur_wtf = form.NomConstructeur_wtf.data

                name_constructeur = name_constructeur_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_NomConstructeur": name_constructeur}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_constructeur = """INSERT INTO t_constructeur (ID_Constructeur,NomConstructeur) VALUES (NULL,%(value_NomConstructeur)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_constructeur, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('constructeurs_afficher', order_by='DESC', ID_Constructeur_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_constructeur_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_constructeur_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion constructeurs CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("constructeurs/Constructeurs_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /constructeur_update
    
    Test : ex cliquer sur le menu "constructeurs" puis cliquer sur le bouton "EDIT" d'un "constructeur"
    
    Paramètres : sans
    
    But : Editer(update) un constructeur qui a été sélectionné dans le formulaire "constructeurs_afficher.html"
    
    Remarque :  Dans le champ "NomConstructeur_update_wtf" du formulaire "constructeurs/Constructeurs_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/constructeurs_update", methods=['GET', 'POST'])
def constructeurs_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_Constructeur"
    ID_Constructeur_update = request.values['ID_Constructeur_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateConstructeurs()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "Constructeurs_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_constructeur_update = form_update.NomConstructeur_update_wtf.data
            name_constructeur_update = name_constructeur_update.lower()

            valeur_update_dictionnaire = {"value_ID_Constructeur": ID_Constructeur_update,
                                          "value_name_constructeur": name_constructeur_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_NomConstructeur = """UPDATE t_constructeur SET NomConstructeur = %(value_name_constructeur)s WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_NomConstructeur, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_Constructeur_update"
            return redirect(url_for('constructeurs_afficher', order_by="ASC", ID_Constructeur_sel=ID_Constructeur_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_Constructeur" et "NomConstructeur" de la "t_constructeur"
            str_sql_ID_Constructeur = "SELECT ID_Constructeur, NomConstructeur FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"
            valeur_select_dictionnaire = {"value_ID_Constructeur": ID_Constructeur_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_Constructeur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'UPDATE
            data_NomConstructeur = mybd_curseur.fetchone()
            print("data_NomConstructeur ", data_NomConstructeur, " type ", type(data_NomConstructeur), " constructeur ",
                  data_NomConstructeur["NomConstructeur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "Constructeurs_update_wtf.html"
            form_update.NomConstructeur_update_wtf.data = data_NomConstructeur["NomConstructeur"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans constructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans constructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans constructeurs_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans constructeurs_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("constructeurs/Constructeurs_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /constructeur_delete
    
    Test : ex. cliquer sur le menu "constructeurs" puis cliquer sur le bouton "DELETE" d'un "constructeur"
    
    Paramètres : sans
    
    But : Effacer(delete) un constructeur qui a été sélectionné dans le formulaire "constructeurs_afficher.html"
    
    Remarque :  Dans le champ "NomConstructeur_delete_wtf" du formulaire "constructeurs/Constructeurs_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/constructeurs_delete", methods=['GET', 'POST'])
def constructeurs_delete_wtf():
    data_NomConstructeur_attribue_constructeur_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_Constructeur"
    ID_Constructeur_delete = request.values['ID_Constructeur_btn_delete_html']

    # Objet formulaire pour effacer le constructeur sélectionné.
    form_delete = FormWTFDeleteConstructeurs()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("constructeurs_afficher", order_by="ASC", ID_Constructeur_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "constructeurs/Constructeurs_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_NomConstructeur_attribue_constructeur_delete = session['data_NomConstructeur_attribue_constructeur_delete']
                print("data_NomConstructeur_attribue_constructeur_delete ", data_NomConstructeur_attribue_constructeur_delete)

                flash(f"Effacer le constructeur de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer constructeur" qui va irrémédiablement EFFACER le constructeur
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_Constructeur": ID_Constructeur_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_NomConstructeur_Constructeur = """DELETE FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
                str_sql_delete_ID_Constructeur = """DELETE FROM t_constructeur WHERE ID_Constructeur = %(value_ID_Constructeur)s"""
                # Manière brutale d'effacer d'abord la "fk_constructeur", même si elle n'existe pas dans la "t_ggenre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_NomConstructeur_Constructeur, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_ID_Constructeur, valeur_delete_dictionnaire)

                flash(f"Constructeur définitivement effacé !!", "success")
                print(f"Constructeur définitivement effacé !!")

                # afficher les données
                return redirect(url_for('constructeurs_afficher', order_by="ASC", ID_Constructeur_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_Constructeur": ID_Constructeur_delete}
            print(ID_Constructeur_delete, type(ID_Constructeur_delete))

            # Requête qui affiche tous les films qui ont le constructeur que l'utilisateur veut effacer
            str_sql_constructeurs_NomConstructeur_delete = """SELECT NomConstructeur FROM t_constructeur
INNER JOIN t_avoircategorieproduit ON t_constructeur.ID_Constructeur = t_avoircategorieproduit.FK_ID_Constructeur
WHERE t_constructeur.ID_Constructeur = %(value_ID_Constructeur)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_constructeurs_NomConstructeur_delete, valeur_select_dictionnaire)
            data_NomConstructeur_attribue_constructeur_delete = mybd_curseur.fetchall()
            print("data_NomConstructeur_attribue_constructeur_delete...", data_NomConstructeur_attribue_constructeur_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "constructeurs/Constructeurs_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_NomConstructeur_attribue_constructeur_delete'] = data_NomConstructeur_attribue_constructeur_delete

            # Opération sur la BD pour récupérer "ID_Constructeur" et "NomConstructeur" de la "t_constructeur"
            str_sql_ID_Constructeur = "SELECT ID_Constructeur, NomConstructeur FROM t_constructeur WHERE ID_Constructeur= %(value_ID_Constructeur)s"

            mybd_curseur.execute(str_sql_ID_Constructeur, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "NomConstructeur" pour l'action DELETE
            data_NomConstructeur = mybd_curseur.fetchone()
            print("data_NomConstructeur ", data_NomConstructeur, " type ", type(data_NomConstructeur), " constructeur ",
                  data_NomConstructeur["NomConstructeur"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "Constructeurs_delete_wtf.html"
            form_delete.NomConstructeur_delete_wtf.data = data_NomConstructeur["NomConstructeur"]

            # Le bouton pour l'action "DELETE" dans le form. "Constructeurs_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans constructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans constructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans constructeurs_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans constructeurs_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("constructeurs/Constructeurs_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_NomConstructeur_associes=data_NomConstructeur_attribue_constructeur_delete)
