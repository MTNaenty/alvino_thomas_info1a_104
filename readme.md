Module 104 Constructeur d'ordinateurs (liste des constructeurs et de leurs types de produits)
---


# Faire fonctionner ce projet :
##### BUT : CRUD (Create Read Update Delete) complet sur la table "t_constructeur"
* Démarrer le serveur MySql (uwamp ou xamp ou mamp, etc)
* Dans PyCharm, importer la BD grâce en exécutant le run "run" du fichier "zzzdemos/1_ImportationDumpSql.py".

* Puis dans le répertoire racine du projet, ouvrir le fichier "1_run_server_flask.py" et faire un "run".
  * Indispensable, car la BD à changé depuis le dernier exercice.
* Choisir le menu "Constructeurs".
* Cliquer sur "AJOUTER" pour ajouter un constructeur
* Cliquer sur "DELETE" pour supprimer un constructeur
* Cliquer sur "EDIT" pour éditer le nom d'un constructeur
